package Test4;

import java.io.*;
import java.util.Enumeration;
import java.util.Vector;

public class Merging {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream=new FileInputStream("C:\\Users\\coditas\\IdeaProjects\\TEST\\src\\Test4\\a.txt");
        FileInputStream fileInputStream2=new FileInputStream("C:\\Users\\coditas\\IdeaProjects\\TEST\\src\\Test4\\b.txt");
        FileInputStream fileInputStream3=new FileInputStream("C:\\Users\\coditas\\IdeaProjects\\TEST\\src\\Test4\\c.txt");
        FileInputStream fileInputStream4=new FileInputStream("C:\\Users\\coditas\\IdeaProjects\\TEST\\src\\Test4\\d.txt");
        FileOutputStream fileOutputStream=new FileOutputStream("intermediate.txt");

        Vector vector=new Vector<>();

        vector.addElement(fileInputStream);
        vector.addElement(fileInputStream2);
        vector.addElement(fileInputStream3);
        vector.addElement(fileInputStream4);

        Enumeration enumeration=vector.elements();

        SequenceInputStream sequenceInputStream=new SequenceInputStream(enumeration);

        int i;
        while ((i=sequenceInputStream.read())!= -1){
            fileOutputStream.write((char)i);
        }
        fileInputStream.close();
        fileInputStream2.close();
        fileInputStream3.close();
        fileInputStream4.close();
        sequenceInputStream.close();
        fileOutputStream.close();
        System.out.println("merging of all file date in one file is done");

        CharArrayWriter charArrayWriter=new CharArrayWriter();
        FileWriter fileWriter=new FileWriter("a1.txt");
        FileWriter fileWriter2=new FileWriter("b1.txt");
        FileWriter fileWriter3=new FileWriter("c1.txt");
        FileWriter fileWriter4=new FileWriter("d1.txt");
        FileReader fileReader=new FileReader("C:\\Users\\coditas\\IdeaProjects\\TEST\\intermediate.txt");

        int j;
        while ((j=fileReader.read())!= -1){
            charArrayWriter.write((char)j);
        }

        charArrayWriter.writeTo(fileWriter);
        charArrayWriter.writeTo(fileWriter2);
        charArrayWriter.writeTo(fileWriter3);
        charArrayWriter.writeTo(fileWriter4);

        fileWriter.close();
        fileWriter2.close();
        fileWriter3.close();
        fileWriter4.close();
        fileReader.close();
        charArrayWriter.close();


        System.out.println("transfer of same data from one file to another 4 files is done");


    }
}
