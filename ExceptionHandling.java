package Test4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

class NumberNOtFound extends Exception{
    @Override
    public String toString() {
        return "Number NOt Found !!!!!";
    }
}

class NumberOutOfRanger extends Exception{
    @Override
    public String toString() {
        return "Number Out Of Ranger !!!!!";
    }
}

public class ExceptionHandling {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter the line");
        String str=bufferedReader.readLine();
        int num = 0;
        int count=0;
        char array[]=str.toCharArray();
        for(int i=0;i<array.length;i++){
            if(Character.isDigit(array[i])){
                num+=array[i];
                count++;
            }
        }
//        System.out.println(num);


        int sum=0;
        int number=num;
        while (number>0){
            int digit=number%10;
            sum=sum+digit;
            number=number/10;

        }

        System.out.println("the sum of the giving number in string " +sum);

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("enter the user number");
        int userNumber=Integer.parseInt(br.readLine());
        System.out.println("the user number is "+userNumber);
        if(userNumber>sum){
            try {
                throw new NumberOutOfRanger();
            } catch (NumberOutOfRanger e) {
                System.out.println(e);
            }
        }else{
            System.out.println("NUmber is in the range ,you can move forward");
            System.out.println("thank you for using");
        }
        if(count==0){
            try {
                throw new NumberNOtFound();
            } catch (NumberNOtFound e) {
                e.printStackTrace();
            }
        }


    }
}
